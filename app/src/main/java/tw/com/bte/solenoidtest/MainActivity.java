package tw.com.bte.solenoidtest;

import androidx.annotation.*;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private InputStream mInputStream;
    private OutputStream mOutputStream;
    private ReadThread mReadThread;
    private Button[] btn = new Button[4];
    private EditText[] txtOntime = new EditText[4];
    private EditText[] txtOfftime = new EditText[4];
    private EditText[] txtTimes = new EditText[4];
    private int[] aryOntime = new int[]{R.id.txtOntime1, R.id.txtOntime2, R.id.txtOntime3, R.id.txtOntime4};
    private int[] aryOfftime = new int[]{R.id.txtOfftimes1, R.id.txtOfftimes2, R.id.txtOfftimes3, R.id.txtOfftimes4};
    private int[] aryTimes = new int[]{R.id.txtTimes1, R.id.txtTimes2, R.id.txtTimes3, R.id.txtTimes4};
    private int[] aryButton = new int[]{R.id.btnStart1, R.id.btnStart2, R.id.btnStart3, R.id.btnStart4};
    private Button btnSendAll;
    private final int MAX = 100;
    private final int TIMES_MAX = 255;
    private InputFilterMinMax[] mInputFilter = new InputFilterMinMax[]{new InputFilterMinMax()};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LayoutInit();
        Uart_Init();
    }

    private void LayoutInit(){
        //txtOntime
        for(int i = 0; i < aryOntime.length; i ++){
            txtOntime[i] = (EditText) findViewById(aryOntime[i]);
            txtOntime[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus == false){
                        EditText mEditText = (EditText)v;
                        int nValue = Integer.parseInt(mEditText.getText().toString());
                        if(nValue > MAX){
                            mEditText.setText(String.valueOf(MAX));
                        } else if(nValue < 0){
                            mEditText.setText(String.valueOf(0));
                        }
                    }
                }
            });
        }

        //txtOfftime
        for(int i = 0; i < aryOfftime.length; i++){
            txtOfftime[i] = (EditText) findViewById(aryOfftime[i]);
            txtOfftime[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus == false){
                        EditText mEditText = (EditText)v;
                        int nValue = Integer.parseInt(mEditText.getText().toString());
                        if(nValue > MAX){
                            mEditText.setText(String.valueOf(MAX));
                        } else if(nValue < 0){
                            mEditText.setText(String.valueOf(0));
                        }
                    }
                }
            });
        }

        //txttimes
        for(int i = 0; i < aryTimes.length; i ++){
            txtTimes[i] = (EditText) findViewById(aryTimes[i]);
            txtTimes[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus == false){
                        EditText mEditText = (EditText)v;
                        int nValue = Integer.parseInt(mEditText.getText().toString());
                        if(nValue > TIMES_MAX){
                            mEditText.setText(String.valueOf(TIMES_MAX));
                        } else if(nValue < 0){
                            mEditText.setText(String.valueOf(0));
                        }
                    }
                }
            });
        }

        //button
        for(int i = 0; i < aryButton.length; i ++){
            btn[i] = (Button) findViewById(aryButton[i]);
            btn[i].setOnClickListener(this);
        }

        btnSendAll = (Button)findViewById(R.id.btnSentAll);
        btnSendAll.setOnClickListener(this);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        int action = v.getId();
        //byte[] data = new byte[]{(byte) 0xa6, 0x01, 0x00};
        //SendUart(data);
        byte[] data = new byte[]{(byte) 0xA6, 0x13, 0x04, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        for(int i = 0; i < aryButton.length; i ++){
            if(action == aryButton[i]){
                int onTime = Integer.parseInt(txtOntime[i].getText().toString());
                if(onTime > MAX){
                    onTime = MAX;
                    txtOntime[i].setText(String.valueOf(MAX));
                }
                int offTime = Integer.parseInt(txtOfftime[i].getText().toString());
                if(offTime > MAX){
                    offTime = MAX;
                    txtOfftime[i].setText(String.valueOf(MAX));
                }
                int times = Integer.parseInt(txtTimes[i].getText().toString());
                if(times > TIMES_MAX){
                    times = TIMES_MAX;
                    txtTimes[i].setText(String.valueOf(TIMES_MAX));
                }
                data[3*i + 3] = (byte)(times & 0xff);
                data[3*i + 4] = (byte)(onTime & 0xff);
                data[3*i + 5] = (byte)(offTime & 0xff);
                Log.d("TEST", String.format("%02x, %02x, %02x", data[3], data[4], data[5]));
                SendUart(data);
            }
        }

        if(action == R.id.btnSentAll){
            for(int j =0; j < 4; j ++){
                int onTime = Integer.parseInt(txtOntime[j].getText().toString());
                if(onTime > MAX){
                    onTime = MAX;
                    txtOntime[j].setText(String.valueOf(MAX));
                }
                int offTime = Integer.parseInt(txtOfftime[j].getText().toString());
                if(offTime > MAX){
                    offTime = MAX;
                    txtOfftime[j].setText(String.valueOf(MAX));
                }
                int times = Integer.parseInt(txtTimes[j].getText().toString());
                if(times > TIMES_MAX){
                    times = TIMES_MAX;
                    txtTimes[j].setText(String.valueOf(TIMES_MAX));
                }
                data[3*j + 3] = (byte)(times & 0xff);
                data[3*j + 4] = (byte)(onTime & 0xff);
                data[3*j + 5] = (byte)(offTime & 0xff);
            }

            String strData = "";
            for(int i =0; i < data.length; i++){
                strData += (String.valueOf(data[i]) + ", ");
            }
            Log.d("TEXT", strData);
            SendUart(data);
        }
    }

    private void SendUart(byte[] data){
        try{
            mOutputStream.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Uart_Init(){
        try {
            SerialPort serialPort = new SerialPort(new File("/dev/ttyS3"), 115200, 0);
            mInputStream = serialPort.getInputStream();
            mOutputStream = serialPort.getOutputStream();
            mReadThread = new ReadThread();
            mReadThread.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class ReadThread extends Thread{
        @Override
        public void run() {
            super.run();
            while(!isInterrupted()){
                int size;
                try {
                    byte[] buffer = new byte[64];
                    if (mInputStream == null) return;
                    size = mInputStream.read(buffer);
                    if (size > 0) {
                        Log.d("UART_R", String.format("%02X %02X %02X %02X %02X %02X %02X %02X %02X", buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5], buffer[6], buffer[7], buffer[8]));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }
}
