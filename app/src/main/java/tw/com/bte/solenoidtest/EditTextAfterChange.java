package tw.com.bte.solenoidtest;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;

public class EditTextAfterChange implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.d("TEXT", "Test change: " + s);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
